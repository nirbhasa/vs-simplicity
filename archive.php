<?php get_header(); ?>

	<div id="content-wrapper">
  <div id="content-wrapper-inside">
  
	<div id="content" class="narrowcolumn">

		<?php if (have_posts()) : ?>

		 <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
<?php /* If this is a category archive */ if (is_category()) { ?>				
		<h2 class="pagetitle"><?php _e('Archive for category','vs-simplicity'); ?> <?php echo single_cat_title(); ?>'</h2>
		
 	  <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		<h2 class="pagetitle"><?php _e('Archive for','vs-simplicity'); ?> <?php the_time('F jS, Y'); ?></h2>
		
	 <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		<h2 class="pagetitle"><?php _e('Archive for','vs-simplicity'); ?> <?php the_time('F, Y'); ?></h2>

		<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		<h2 class="pagetitle"><?php _e('Archive for','vs-simplicity'); ?> <?php the_time('Y'); ?></h2>
		
	  <?php /* If this is a search */ } elseif (is_search()) { ?>
		<h2 class="pagetitle"><?php _e('Search results','vs-simplicity'); ?></h2>
		
	  <?php /* If this is an author archive */ } elseif (is_author()) { ?>
		<h2 class="pagetitle"><?php _e('Author Archive','vs-simplicity'); ?></h2>

		<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		<h2 class="pagetitle"><?php _e('Blog Archive','vs-simplicity'); ?></h2>

		<?php } ?>

		<div class="navigation">
			<div class="alignleft"><?php posts_nav_link('','','&laquo; Previous entries') ?></div>
			<div class="alignright"><?php posts_nav_link('','Next entries &raquo;','') ?></div>
			<div class="clear"></div>
		</div>

       <div id="archive-list">
          <ul class="front-item-list">
       		   <?php if ( have_posts() ) {
				       while ( have_posts() ) { 
				        the_post(); 
				        $image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail');
				        ?>

				       <li><span>
                         <a href="<?php the_permalink(); ?>"><img src="<?php echo $image_url[0]; ?>" alt="<?php the_title(); ?>"/></a>
                         <div class="column-text">
				             <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
				             <?php the_excerpt(); ?> <a class="readmore" href="<?php the_permalink(); ?>">Read more »</a>
				        </div>
				        <div class="clear"></div>
				      </span></li>

			       <?php } } else { ?>
			            <p class="center"><?php _e('Unfortunately no items exist for this month.','ckg-media'); ?></p>
		          <?php } ?>
		    </ul>   
		    <div class="clear"></div>   
	    </div>	          
		
		<div class="navigation">
			<div class="alignleft"><?php next_posts_link(__('&laquo; Older entries','vs-simplicity'),0); ?></div>
			<div class="alignright"><?php previous_posts_link(__('Newer entries &raquo;','vs-simplicity'),0) ?></div>
			<div class="clear"></div>
		</div>
		
	<?php else : ?>

		<h2 class="center"><?php _e('No content was found for that time period. Perhaps searching will help.','vs-simplicity'); ?></h2>
		<?php include (TEMPLATEPATH . '/searchform.php'); ?>

	<?php endif; ?>
		
	</div><!-- #content -->
	
   <?php get_sidebar('articles'); ?>
   <div class="clear"></div>
  </div><!-- #content-wrapper-inside -->
</div><!-- #content-wrapper -->

<?php get_footer(); ?>