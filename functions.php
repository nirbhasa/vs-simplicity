<?php

function simplicity_custom_scripts() {
   wp_enqueue_script( 'custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), FALSE, TRUE);
}
add_action('wp_enqueue_scripts', 'simplicity_custom_scripts');

// Load up the theme options

if (!function_exists('simplicity_init_options')) {
    function simplicity_init_options() {
	$options['hide_search'] = FALSE;
	$options['banner_format'] = 'image';
	$options['googlefonts'] = array('Open+Sans:300');
	update_option('vs-simplicity-options', $options);
    }
}    
add_action('init', 'simplicity_init_options');	

if (!function_exists('simplicity_setup')) {
 function simplicity_setup() {
    
    /* Note: can also use this to generate headers for different mobile sizes */
    add_theme_support( 'post-thumbnails' );  
    add_image_size('slideshow-half', 400, 240, true);
    add_image_size('slideshow-full', 710, 240, true); 

    // Add support for custom header
    $defaults = array(
	  'default-image'          => get_stylesheet_directory_uri() . '/images/logo.jpg',
	  'random-default'         => false,
	  'header-text'            => false,
	  'uploads'                => true,
   );
   add_theme_support( 'custom-header', $defaults );
  }
}

function simplicity_common_setup() {    
    // Make theme available for translation
	// Translations can be filed in the /languages/ directory
	load_theme_textdomain( 'vs-simplicity', TEMPLATEPATH . '/languages' );

	$locale = get_locale();
	$locale_file = TEMPLATEPATH . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );

	// This theme uses wp_nav_menu() 
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'vs-simplicity' ),
		'secondary' => __( 'Secondary Navigation', 'vs-simplicity' ),
		/* 'footer' => __( 'Footer Navigation', 'vs-simplicity' ), */
	) );	
}
add_action('after_setup_theme', 'simplicity_setup');
add_action('after_setup_theme', 'simplicity_common_setup');

function simplicity_widgets_init() {
if ( function_exists('register_sidebar') ) {

  register_sidebar(array(
    'name' => __( 'Right Sidebar - Pages (Default)', 'vs-simplicity' ),
    'id' => 'sidebar-1',
    'before_widget' => '<div id="%1$s" class="sidebar-widget clearfix">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>',  
  ));
  
  register_sidebar(array(
    'name' => __( 'Right Sidebar - Articles', 'vs-simplicity' ),
	'id' => 'sidebar-articles',
    'before_widget' => '<div id="%1$s" class="sidebar-widget clearfix">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>',  
  )); 

  register_sidebar(array(
    'name' => __( 'Front page featured widget 1', 'vs-simplicity' ),
	'id' => 'featured-1',
    'before_widget' => '<div id="%1$s" class="featured-widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ));
  
  register_sidebar(array(
    'name' => __( 'Front page featured widget 2', 'vs-simplicity' ),
	'id' => 'featured-2',
    'before_widget' => '<div id="%1$s" class="featured-widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ));
  
  register_sidebar(array(
    'name' => __( 'Front page featured widget 3', 'vs-simplicity' ),
	'id' => 'featured-3',
    'before_widget' => '<div id="%1$s" class="featured-widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ));

  register_sidebar(array(
    'name' => __( 'Footer widget 1', 'vs-simplicity' ),
	'id' => 'footer-1',
    'before_widget' => '<div id="%1$s" class="footer-widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ));
  
  register_sidebar(array(
    'name' => __( 'Footer widget 2', 'vs-simplicity' ),
	'id' => 'footer-2',
    'before_widget' => '<div id="%1$s" class="footer-widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ));
  
  register_sidebar(array(
    'name' => __( 'Footer widget 3', 'vs-simplicity' ),
	'id' => 'footer-3',
    'before_widget' => '<div id="%1$s" class="footer-widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ));
  
  register_sidebar(array(
    'name' => __( 'Colophon widget 1', 'vs-simplicity' ),
	'id' => 'colophon-1',
    'before_widget' => '<div id="%1$s" class="colophon-widget">',
    'after_widget' => '</div>',
    'before_title' => '<h4>',
    'after_title' => '</h4>',
  ));
  
  register_sidebar(array(
    'name' => __( 'Colophon widget 2', 'vs-simplicity' ),
	'id' => 'colophon-2',
    'before_widget' => '<div id="%1$s" class="colophon-widget">',
    'after_widget' => '</div>',
    'before_title' => '<h4>',
    'after_title' => '</h4>',
  ));  
  
  register_sidebar(array(
    'name' => __( 'Right Sidebar - Pages 2', 'vs-simplicity' ),
	'id' => 'sidebar-2',
    'before_widget' => '<div id="%1$s" class="sidebar-widget clearfix">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>',  
  )); 
  
  register_sidebar(array(
    'name' => __( 'Right Sidebar - Pages 3', 'vs-simplicity' ),
	'id' => 'sidebar-3',
    'before_widget' => '<div id="%1$s" class="sidebar-widget clearfix">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>',  
  )); 
  
  register_sidebar(array(
    'name' => __( 'Right Sidebar - Pages 4', 'vs-simplicity' ),
	'id' => 'sidebar-4',
    'before_widget' => '<div id="%1$s" class="sidebar-widget clearfix">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>',  
  )); 
  
  register_sidebar(array(
    'name' => __( 'Right Sidebar - Pages 5', 'vs-simplicity' ),
	'id' => 'sidebar-5',
    'before_widget' => '<div id="%1$s" class="sidebar-widget clearfix">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>',  
  ));   


 }
}
/** Register sidebars by running vs-simplicity_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'simplicity_widgets_init' );



/** Disable Wordpress best guess redirection **/

// Disable this for now
// add_filter('redirect_canonical', 'no_redirect_on_404');
function no_redirect_on_404($redirect_url)
{
    if (is_404()) {
        return false;
    }
    return $redirect_url;
}


function simplicity_breadcrumb() {
    global $post;
    global $wp_query, $wpdb;

    $output = '';

    $current_post_id = $wp_query->get_queried_object_id();
    $current_post = get_post($current_post_id);      
    $ancestors = array_reverse(get_post_ancestors($current_post));
    
    if(!empty($ancestors)) {
        
      foreach ($ancestors as $ancestor) {
        $output .= '<a href="' . get_permalink( $ancestor ) . '">' . get_the_title($ancestor) . '</a> » ';
      }  
      $output .= get_the_title($current_post_id); 
    
      return '<div id="breadcrumb">' . $output . '</div>';  
    }  
 
}   


/* Multilingual dropdown widget */
function simplicity_language_switcher() {
 if(defined('ICL_SITEPRESS_VERSION')) {
      do_action('icl_language_selector');
 }    
}


function simplicity_banner() {
   
   $options = get_option('vs-simplicity-options'); 
   $heading_tag = ( is_home() || is_front_page() ) ? 'h1' : 'h2'; 

   switch($options['banner_format']) {
    
    case 'title':
    
        return '<' . $heading_tag . ' id="site-title"><a href="/" title="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '" rel="home">' . esc_attr(get_bloginfo('name')) . '</a></' . $heading_tag . '>';
    
    
    case 'both':
        $textcolor = get_header_textcolor();
        $coloroutput = '';
        if(!empty($textcolor)) {
          $coloroutput = '#header { background-color: #' . get_header_textcolor() . '; } #front-featured { background-color: #' . get_header_textcolor() . '; } ';
        }

        return '<' . $heading_tag . ' id="site-title"><a href="/" title="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '" rel="home">'. esc_attr(get_bloginfo( 'name', 'display')) .'</a></' . $heading_tag . '><style type="text/css">#header-inside { background-image: url("'. get_header_image() . '"); background-repeat: no-repeat; } body.front-slideshow #header-inside { background-image /*\**/: none\9 } ' . $coloroutput . '</style>';  
        

    case 'image':
    default:
    
        /*  SEO header image thing 
            do h1 on homepage, h2 on post pages (the page title is h1 there) */ 
         return '<' . $heading_tag . ' id="site-title"><a href="/" title="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '" rel="home"><img src="'. get_header_image() . '" alt="'. esc_attr( get_bloginfo( 'name', 'display' ) ) . '" /></a></'. $heading_tag . '>';   

		  
   }  
} 

function simplicity_body_classes() {
   $classes = array();
   
   /* Add front page class */
   if(is_front_page()) { $classes[] = "front"; }
   
   /* Add class if slides exist for slideshow */
   $slides = get_posts(array('wpss_sliders' => 'front-page')); 
   $slides = get_posts(array('post_type' => 'wpss_slides', 'post_status' => 'publish')); 
     
   if(!empty($slides) && is_front_page()) {
     $classes[] = "front-slideshow";
   } 
   
   /* Add class if we have our front featured widgets */
   if (   is_front_page() && (is_active_sidebar( 'featured-1'  )
		|| is_active_sidebar( 'featured-2' )
		|| is_active_sidebar( 'featured-3'  ))) {
	$classes[] = "front-featured";	
   }
   
   if (!empty($classes)) {
      return ' class="' . implode(' ', $classes) . '"';
   }     
}    

function simplicity_googlefonts() {
  $options = get_option('vs-simplicity-options');
  $fonts = $options['googlefonts'];
  
  if(!empty($fonts)) {
     $fontstring = implode('|' , $fonts);
     
     /* $url = 'http://fonts.googleapis.com';
     list($status) = get_headers($url); */
     if (/* strpos($status, '200') !== FALSE && */ !empty($fontstring)) {
       return "<link href='http://fonts.googleapis.com/css?family=" . $fontstring ."' rel='stylesheet' type='text/css'>";
     }
  }   
  return '';
    
}

function simplicity_slide_edit_link($ID) {
   if ( current_user_can('edit_post', $ID) ) {
     echo '<div class="edit-slide-link"><a href="' . get_edit_post_link($ID) . '">(Edit slide)</a></div>';
   }
}

/* Prints out all posts in a particular term
currently used for sites vocab on help.vasudevaserver.org */
function showPostsByCat($term,$showParentDetails = 1) {
 $header = NULL;

 $args = array(
 'type'                     => 'post',
 'orderby'                  => 'order',
 'order'                    => 'ASC',
 'hide_empty'               => true,
 'hierarchical'             => 1,
 'exclude'                  => NULL,
 'include'                  => NULL,
 'number'                   => NULL,
 'pad_counts'               => false,
 'title_li'                 => '' );

 //Defines an array of the sub categories
 $Cats = get_categories($args);

 //Create a container for the whole output just to ensure that CSS works and that no flaots muck it up.
 ?><div class="category-main"><?

 //This is the main loop for outputiong the information for each sub category
 foreach($Cats as $thisSubCat) {

 //Now we start the loop for posts within this particular category
 rewind_posts();
 
 $post_ids = get_objects_in_term( $term, 'sites' ) ;
 
 $args = array(
 'cat'                     => $thisSubCat->cat_ID,
 'tax_query'                =>  array(array('taxonomy' => 'sites', 'terms' => array($term), 'field' => 'slug')),
 'orderby'                  => 'order',
 'order'                    => 'ASC',
 'hide_empty'               => true,
 'hierarchical'             => 0,
 'exclude'                  => NULL,
 'include'                  => NULL,
 'number'                   => NULL,
 'pad_counts'               => false,
 'title_li'                 => '' );
 
query_posts($args);

 if ( have_posts() ) : 
 
  print "<ul class=\"catlisting\"><li><h3>".$thisSubCat->name."</h3>";
 if ($thisSubCat->description != '') {
 print "<p class=\"category-description\">".$thisSubCat->description."</p>";
 }
 print '<ul class="subcatposts">';
 while ( have_posts() ) : the_post();

        print '<li><a href="' . get_permalink() . '">'. get_the_title() . '</a></li>';

 endwhile; else: ?>

 <?php endif; ?>

 <?
 print "</ul></li>";

 }

 ?></div><?
}



/* nice debug function to print 
   out big arrays when testing */
function print_r_tree($data)
{
    // capture the output of print_r
    $out = print_r($data, true);

    // replace something like '[element] => <newline> (' with <a href="javascript:toggleDisplay('...');">...</a><div id="..." style="display: none;">/Users/nirbhasa/Desktop/centre snag list (Autosaved).rtf
    $out = preg_replace('/([ \t]*)(\[[^\]]+\][ \t]*\=\>[ \t]*[a-z0-9 \t_]+)\n[ \t]*\(/iUe',"'\\1<a href=\"javascript:toggleDisplay(\''.(\$id = substr(md5(rand().'\\0'), 0, 7)).'\');\">\\2</a><div id=\"'.\$id.'\" style=\"display: none;\">'", $out);

    // replace ')' on its own on a new line (surrounded by whitespace is ok) with '</div>
    $out = preg_replace('/^\s*\)\s*$/m', '</div>', $out);

    // print the javascript function toggleDisplay() and then the transformed output
    echo '<pre><script language="Javascript">function toggleDisplay(id) { document.getElementById(id).style.display = (document.getElementById(id).style.display == "block") ? "none" : "block"; }</script>'."\n$out".'</pre>';
}


function simplicity_admin_js() {
    $url = get_option('siteurl');
    $url = get_bloginfo('template_directory') . '/js/wp-admin.js';
    echo '"<script type="text/javascript" src="'. $url . '"></script>"';
}
add_action('admin_footer', 'simplicity_admin_js');

function simplicity_theme_admin_notice(){
 
   if ($_GET['taxonomy'] == 'wpss_sliders' && $_GET['action'] == 'edit' && $_GET['post_type'] == 'wpss_slides') {
      echo '<div class="updated">
       <p>Note: For some sliders, particularly the front page slider, the width and height fields may be overridden by the theme.</p>
      </div>';
  }
}
add_action('admin_notices', 'simplicity_theme_admin_notice');





?>