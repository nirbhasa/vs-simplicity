<?php
/*
Template Name: Page, with Right sidebar 2
*/
?>

<?php get_header(); ?>

  <div id="content-wrapper">
  <div id="content-wrapper-inside">
	<div id="content" class="narrowcolumn">
	
	 <?php echo simplicity_breadcrumb(); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<article>
		<?php if (strlen(get_the_title()) > 0) : ?>
		   <header><h1 class="page-title"><?php the_title(); ?> <?php edit_post_link(__('<span class="edit">(Edit)</span>','vs-simplicity'),'',''); ?></h1></header>
                <?php endif; ?>

			<div class="entry-content">
			
				<?php the_content('<p class="serif">Read more &raquo;</p>'); ?>
	
				<?php wp_link_pages(); ?>
				
				
			   <div class="clear"></div>
	
			</div>
		</article>	
		</div>
	  <?php endwhile; endif; ?>
	  
	</div>

<?php get_sidebar('right-2'); ?>
<div class="clear"></div>
</div></div>

<?php get_footer(); ?>