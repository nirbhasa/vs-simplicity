<?php get_header(); ?>
       
  <div id="content-wrapper">
  <div id="content-wrapper-inside">
	<div id="content" class="narrowcolumn" role="main">

				<h1 class="cat-title"><?php echo single_cat_title( '', false ); ?></h1>
				<?php
					$category_description = category_description();
					if ( ! empty( $category_description ) )
						echo '<div id="category-description">' . $category_description . '</div>';   
						// Get taxonomy query object
                        $taxonomy_archive_query_obj = $wp_query->get_queried_object();                          

                        showPostsByCat($taxonomy_archive_query_obj->slug); 
                            ?>
   			</div><!-- #content -->
		      

      <?php get_sidebar( 'articles' ); ?> 
      <div class="clear"></div>
   </div><!-- #content-wrapper-inside -->
</div><!-- #content-wrapper -->        
	
<?php get_footer(); ?>