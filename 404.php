<?php get_header(); ?>

<div id="content-wrapper">
  <div id="content-wrapper-inside">
	<div id="content" role="main">

			<div id="post-0" class="post error404 not-found">
				<h1 class="entry-title"><?php _e( 'Not Found', 'vs-simplicity' ); ?></h1>
				<div class="entry-content">
					<p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', 'vs-simplicity' ); ?></p>
					<form method="get" id="404-searchform" action="/index.php">
<input type="text" title="<?php _e('Search','vs-simplicity'); ?>" value="<?php _e('Search','vs-simplicity'); ?>" onblur="if(this.value=='') {this.value='<?php _e('Search','vs-simplicity'); ?>';}" onfocus="if(this.value=='<?php _e('Search','vs-simplicity'); ?>') {this.value='';}" name="s" id="404s" maxlength="255" />
</form> 
				</div><!-- .entry-content -->
			</div><!-- #post-0 -->
			
			
	</div><!-- #content -->
   </div><!-- #content-wrapper-inside -->
</div><!-- #content-wrapper -->
		
<script type="text/javascript">
	// focus on search field after it has loaded
	document.getElementById('404s') && document.getElementById('404s').focus();
</script>

<?php get_footer(); ?>