<?php
/**
 * The template for displaying Category Archive pages.
 */

get_header(); ?>

<div id="content-wrapper">
  <div id="content-wrapper-inside">
	<div id="content" class="narrowcolumn" role="main">

				<h1 class="page-title"><?php echo ucwords(single_cat_title( '', false )); ?></h1>
				<?php
					$category_description = category_description();
					if ( ! empty( $category_description ) )
						echo '<div id="category-description">' . $category_description . '</div>'; ?>
						
			   <div id="category-list">
        
                   <div id="category-list-posts">
                  
                  <?php if ( have_posts() ) : ?>

				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', get_post_format() ); ?>
				<?php endwhile; ?>


			<?php else : ?>

				<article id="category-0" class="category no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'vs-simplicity' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'vs-simplicity' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>
                  
                  
		         <div class="clear"></div>
		       </div>  
		    
		    </div>
		    

			</div><!-- #content -->		

     <?php get_sidebar('articles'); ?>
     <div class="clear"></div>
    </div><!-- #content-wrapper-inside -->	
</div><!-- #content-wrapper -->
<?php get_footer(); ?>
