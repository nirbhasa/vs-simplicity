
<?php
	/* 
	 * If none of the sidebars have widgets, then let's bail early.
	 */
	if (   ! is_active_sidebar( 'colophon-1'  )
		&& ! is_active_sidebar( 'colophon-2' )
	)
		return;

?>
<div id="colophon">
    <div id="colophon-inside">
	<?php if ( is_active_sidebar( 'colophon-1' ) ) : ?>
	<div id="colophon-first" class="widget-area" role="complementary">
		<?php dynamic_sidebar( 'colophon-1' ); ?>
	</div><!-- #first .widget-area -->
	<?php endif; ?>

	<?php if ( is_active_sidebar( 'colophon-2' ) ) : ?>
	<div id="colophon-second" class="widget-area" role="complementary">
		<?php dynamic_sidebar( 'colophon-2' ); ?>
	</div><!-- #second .widget-area -->
	<?php endif; ?>
  </div>
</div><!-- #colophon -->