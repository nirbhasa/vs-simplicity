<?php
/**
 * The template for displaying Category Archive pages.
 */

get_header(); ?>

<div id="content-wrapper">
  <div id="content-wrapper-inside">
	<div id="content" class="narrowcolumn" role="main">

				<h1 class="single-post-title"><?php echo ucwords(single_cat_title( '', false )); ?></h1>
				<?php
					$category_description = category_description();
					if ( ! empty( $category_description ) )
						echo '<div id="category-description">' . $category_description . '</div>'; ?>
						
			   <div id="category-list">
       
                   <h4 class="category-list-header"><?php echo ucwords(single_cat_title( '', false )); ?> items</h4>
                   
                   <div id="category-list-posts">
                   <ul class="front-item-list">

				     <?php if ( have_posts() ) {
				       while ( have_posts() ) { 
				        the_post(); 
				        $image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail');
				        ?>

				       <li><span>
                         <a href="<?php the_permalink(); ?>"><img src="<?php echo $image_url[0]; ?>" alt="<?php the_title(); ?>"/></a>
                         <div class="column-text">
				             <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
				             <?php the_excerpt(); ?> <a class="readmore" href="<?php the_permalink(); ?>">Read more »</a>
				        </div>
				        <div class="clear"></div>
				      </span></li>

			       <?php } } else { ?>
			            <p class="center"><?php _e('Unfortunately nothing has been yet posted under this category.','ckg-media'); ?></p>
		          <?php } ?>
		          </ul>
		         <div class="clear"></div>
		       </div>  
		    
		    </div>
		    

			</div><!-- #content -->		

     <?php get_sidebar('articles'); ?>
     <div class="clear"></div>
    </div><!-- #content-wrapper-inside -->	
</div><!-- #content-wrapper -->
<?php get_footer(); ?>
