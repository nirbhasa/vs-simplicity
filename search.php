<?php get_header(); ?>

	<div id="content-wrapper">
  <div id="content-wrapper-inside">
  
	<div id="content" class="narrowcolumn">

	<?php if (have_posts()) : ?>

		<h2 class="pagetitle"><?php _e('Search results for ','vs-simplicity'); ?> <?php /* Search Count */ $allsearch = &new WP_Query("s=$s&showposts=-1"); $key = wp_specialchars($s, 1); $count = $allsearch->post_count; _e(''); _e('<span class="search-terms">"'); echo $key; _e('"</span>'); ?></h2>
		<strong>Found <?php _e(' — '); echo $count . ' '; _e('articles'); wp_reset_query(); ?></strong>
		
		<div class="search-navigation">
			<div class="alignleft"><?php posts_nav_link('','',__('&laquo; Previous entries','vs-simplicity')) ?></div>
			<div class="alignright"><?php posts_nav_link('',__('Next entries &raquo;','vs-simplicity'),'') ?></div>
			<div class="clear"></div>
		</div>


		<?php while (have_posts()) : the_post(); ?>
				
			<div class="search-item">
				<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h3>
				
				<p><?php print substr($post->post_excerpt, 0,150); ?>…</p>
		
				<!--
				<?php trackback_rdf(); ?>
				-->
				<div class="clear"></div>
			</div>
	
		<?php endwhile; ?>

		<div class="search-navigation">
			<div class="alignleft"><?php posts_nav_link('','',__('&laquo; Previous entries','vs-simplicity')) ?></div>
			<div class="alignright"><?php posts_nav_link('',__('Next entries &raquo;','vs-simplicity'),'') ?></div>
			<div class="clear"></div>
		</div>
	
	<?php else : ?>

		<h2 class="center"><?php _e('Not found!','vs-simplicity'); ?></h2>
		<p class="center"><?php _e('No results found. Please try again with other search terms.','vs-simplicity'); ?></p>
		<?php include (TEMPLATEPATH . '/searchform.php'); ?>
		

	<?php endif; ?>
	</div>
		
	  <?php get_sidebar('articles'); ?>
   <div class="clear"></div>
  </div><!-- #content-wrapper-inside -->
</div><!-- #content-wrapper -->

<?php get_footer(); ?>