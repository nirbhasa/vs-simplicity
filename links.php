<?php
/*
Template Name: Links
*/
?>

<?php get_header(); ?>

<div id="content" class="widecolumn">

<h2><?php _e('Links:','yy_minimalismus'); ?></h2>
<ul>
<?php get_links_list(); ?>
</ul>

</div>	

<?php get_footer(); ?>
