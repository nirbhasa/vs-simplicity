
</div> <!-- #page -->


  <div id="footer">
  <footer role="contentinfo">

  <?php if ( ! is_404() ) get_sidebar( 'footer' ); ?>

    <?php get_sidebar( 'colophon' ); ?>          	

  <div class="clear"></div>
  </footer>
  </div><!-- #footer -->
  

<?php do_action('wp_footer'); ?>

</body>

</html>