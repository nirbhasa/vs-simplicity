<div id="sidebar">
	<?php if (is_active_sidebar( 'sidebar-articles' ) ) : ?>
		<?php dynamic_sidebar( 'sidebar-articles' ); ?>
    <?php endif; ?>
</div>