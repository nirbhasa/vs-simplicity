<?php
//Get The ID of the post thumbnail
$img_id = get_post_thumbnail_id( $post->ID );
$link_title = get_post_meta($post->ID, 'slide_link_text', TRUE); 
$link_href = get_post_meta($post->ID, 'slide_link', TRUE);
$image_class = get_post_meta($post->ID, 'slide_image_class', TRUE);

//Then pull the source of the fullsize image
$imgsrc = wp_get_attachment_image_src( $img_id , 'slideshow-half' );
$imgclass = !empty($image_class) ? 'class = "' . $image_class . '" ' : '';
$img = empty($link_href) ? '<img src="' . $imgsrc[0] . '" alt="" />' : '<a href="' . $link_href . '"><img src="' . $imgsrc[0] . '" alt="" ' . $imgclass . '/></a>';

?>
<div class="wpss_content_half_right wpss_content_half">
    <h2><?php the_title(); ?></h2>
    <?php wpss_the_content(); 
    
      if(!empty($link_title) && !empty($link_href)) {
         print '<a href="' . $link_href . '" class="slider-read-more">' . $link_title . '</a>';
      }  
    ?>
    
</div>
<div class="wpss_img_half_right wpss_img_half">
    <?php echo $img; ?>
</div>
<?php simplicity_slide_edit_link($post->ID); ?>
<div class="clear"></div>