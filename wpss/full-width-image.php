<?php
//Get The ID of the post thumbnail
$img_id = get_post_thumbnail_id( $post->ID );
$link_href = get_post_meta($post->ID, 'slide_link', TRUE);
$image_class = get_post_meta($post->ID, 'slide_image_class', TRUE);

//Then pull the source of the fullsize image
$imgsrc = wp_get_attachment_image_src( $img_id , 'slideshow-full' );
$imgclass = !empty($image_class) ? 'class = "' . $image_class . '" ' : '';
$img = empty($link_href) ? '<img src="' . $imgsrc[0] . '" alt="" />' : '<a href="' . $link_href . '"><img src="' . $imgsrc[0] . '" alt="" ' . $imgclass . '/></a>';
?>
<div class="wpss_img_full">
    <?php echo $img; ?>
</div>
