<?php 
      get_header();
?>

  <div id="content-wrapper">
  <div id="content-wrapper-inside">
  
	<div id="content" class="narrowcolumn">
	
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
			<article>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <?php if (strlen(get_the_title()) > 0) : ?>
		        
		        <header>
		          <h1 class="entry-title"><?php the_title(); ?></h1>

                          <div class="entry-meta">
			    <span class="datetime">
					      Created <?php the_time('F j, Y') ?> by <?php the_author(); ?> <?php edit_post_link(__('Edit','vs-simplicity'),' <strong>|</strong> ',''); ?>  
			    </span> 
			    <?php if ( comments_open() && ! post_password_required() ) : ?>
                                 - <span class="comments-link">
				          <?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a reflection', 'vs-simplicity' ) . '</span>', '1 ' . __('reflection', 'vs-simplicity' ), '% ' . __('reflections', 'vs-simplicity' ) ); ?>
			         </span>
			    <?php endif; ?>  
		            </div> 
	                </header>      
                    <?php endif; ?>

             
				<div class="entry">
				    
					
				  <div class="entry-content">
				    <?php the_content(__('<p class="serif">Read more &raquo;', vs-simplicity)); ?>
				  </div>

				  <?php link_pages(__('<p><strong>Pages:</strong> ', vs-simplicity), '</p>', 'number'); ?>
	
				</div>
				
				 
                  
                <div class="clear"></div>
                
                
               <footer><div class="entry-meta">
				    <div class="category-single"><?php _e('See more content categorised under:','vs-simplicity'); ?> <?php the_category(', '); ?></div>
				    
				    <?php /* Todo: apparently these functions are deprecated, should use previous_post_link instead */ ?>		 
				    <div class="alignleft"><?php previous_post('&laquo; Previous: %','','yes') ?></div>
				    <div class="alignright"><?php next_post('Next: % &raquo;','','yes') ?></div>
					
					<div class="clear"></div>
				</div></footer> 
                <?php wp_link_pages(); ?>
             </article>   

			<?php comments_template(); ?>
	
			<!--
				<?php trackback_rdf(); ?>
			-->
        </div> 

		<?php endwhile; else: ?>
	
			<p class="center"><?php _e('The content, which you are looking for, does (no longer) exist.','vs-simplicity'); ?></p>
	
		<?php endif; ?>
	
	</div>
	
<?php get_sidebar('articles'); ?>
<div class="clear"></div>
</div></div>
<?php get_footer(); ?>