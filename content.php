	<article><div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="entry-header"><header> 
		   
			<?php if ( is_sticky() ) : ?>
				<hgroup>
					<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'vs-simplicity' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					<h3 class="entry-format"><?php _e( 'Featured', 'vs-simplicity' ); ?></h3>
				</hgroup>
			<?php else : ?>
			<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'vs-simplicity' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
			<?php endif; ?>

			<?php if ( 'post' == get_post_type() ) : ?>
			 <div class="entry-meta">
			 	        <span class="datetime">
					      Created <?php the_time('F j, Y') ?> by <?php the_author(); ?><?php edit_post_link(__('Edit','vs-simplicity'),' <strong>|</strong> ',''); ?>  
					    </span>   
					    <?php if ( comments_open() && ! post_password_required() ) : ?>
                          - <span class="comments-link">
				          <?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a reflection', 'vs-simplicity' ) . '</span>', _x( '1', 'reflection', 'vs-simplicity' ), _x( '%', 'reflections', 'vs-simplicity' ) ); ?>
			              </span>
			             <?php endif; ?> 
		     </div> 
			<?php endif; ?>
		</header></div><!-- .entry-header -->

		<?php if ( is_search() ) : // Only display Excerpts for Search ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
		<?php else : ?>
		<div class="entry-content">
			<?php the_content( __( 'Continue reading <span class="meta-nav">&raquo;</span>', 'vs-simplicity' ) ); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'vs-simplicity' ) . '</span>', 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->
		<?php endif; ?>

		<footer><div class="entry-meta footer">
			<?php $show_sep = false; ?>
			<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
			<?php
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( __( ', ', 'vs-simplicity' ) );
				if ( $categories_list ):
			?>
			<span class="cat-links">
				<?php printf( __( '<span class="%1$s">View more posts categorised under</span> %2$s', 'vs-simplicity' ), 'entry-utility-prep entry-utility-prep-cat-links', $categories_list );
				$show_sep = true; ?>
			</span>
			<?php endif; // End if categories ?>
			<?php
				/* translators: used between list items, there is a space after the comma */
				$tags_list = get_the_tag_list( '', __( ', ', 'vs-simplicity' ) );
				if ( $tags_list ):
				if ( $show_sep ) : ?>
			<span class="sep"> | </span>
				<?php endif; // End if $show_sep ?>
			<span class="tag-links">
				<?php printf( __( '<span class="%1$s">Tagged</span> %2$s', 'vs-simplicity' ), 'entry-utility-prep entry-utility-prep-tag-links', $tags_list );
				$show_sep = true; ?>
			</span>
			<?php endif; // End if $tags_list ?>
			<?php endif; // End if 'post' == get_post_type() ?>

		 </div></footer><!-- #entry-meta -->
	</div></article><!-- #post-<?php the_ID(); ?> -->
