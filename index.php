<?php get_header(); ?>

      <div id="content-wrapper">
  <div id="content-wrapper-inside">
		<div id="content" class="index narrowcolumn">

			<?php if ( have_posts() ) : ?>

				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', get_post_format() ); ?>
				<?php endwhile; ?>
				
				<div class="navigation">
                   <div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
                   <div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
                </div>


			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'vs-simplicity' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'vs-simplicity' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>

			</div><!-- #content -->
			
<?php get_sidebar('articles'); ?>
<div class="clear"></div>
</div></div>
<?php get_footer(); ?>