<!DOCTYPE html>

<head profile="http://gmpg.org/xfn/11">

	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


	<title>	<?php bloginfo('name'); ?> <?php wp_title(); ?></title>
	<?php echo simplicity_googlefonts(); ?>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
	
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.png" />

	<?php wp_get_archives('type=monthly&format=link'); ?>

	<?php wp_head(); ?>
	
	<?php load_theme_textdomain('vs-simplicity'); ?> 

</head>

<body<?php print simplicity_body_classes(); ?>>

	<div id="wrapper">   
	   <div id="header">
	      <div id="header-inside">
            <div id="banner">
           
	           <?php echo simplicity_banner(); 	           
	           ?>   
            
              <div id="right-header-elements">
                <div id="secondary-navigation"><?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'secondary', 'fallback_cb' => false ) ); ?></div>
			    <div id="search-form"><?php include (TEMPLATEPATH . '/searchform.php'); ?></div>
			   
			  </div> <!-- right-header-elements -->    
			  <div class="clear"></div>
			</div>
			<div id="primary-navigation" class="clearfix" role="navigation">
			  <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
				<!-- <div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'vs-simplicity' ); ?>"><?php _e( 'Skip to content', 'vs-simplicity' ); ?></a></div> -->
				<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
			</div><!-- #navigation -->
			
			<?php if( function_exists( 'wp_slick_slider') && is_front_page() ){  wp_slick_slider( 'front-page' );  } ?>
	
	     <div class="clear"></div>

	  </div></div> <!-- #header, #header-inside -->
	  <div class="clear"></div>
	     <?php if( is_front_page() ){ get_sidebar('featured'); } /* Featured items */ ?>
	  <div class="clear"></div>
	  	  