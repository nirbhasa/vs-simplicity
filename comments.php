<?php // Do not delete these lines
	if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

        if (!empty($post->post_password)) { // if there's a password
            if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie
				?>
				
				<p class="nocomments"><?php _e('This article is password-protected. Please enter the password to view the comments.','ckg-media'); ?><p>
				
				<?php
				return;
            }
        }

		/* This variable is for an other comment background for the admin */
		$oddcomment = '';
?>

<!-- You can start editing here. -->

<?php

$commentdata = array();
foreach ($comments as $comment) {
    if (get_comment_type() == 'comment') {
    	$commentdata['comments'][] = $comment;
    } else {
        $commentdata['trackbacks'][] = $comment;
    }
}

$count_comments   = count($commentdata['comments']);
$count_trackbacks = count($commentdata['trackbacks']);

?>

<?php if ($comments) : ?>

<h3 id="comment-number"> 
<?php

if ($count_comments == 0) {
    $count_comments = __('No comments','ckg-media');
} elseif ($count_comments == 1) {
    $count_comments = __('1 comment','ckg-media');
} else {
    $count_comments = $count_comments . __(' comments','ckg-media');
}

echo $count_comments;

?>
</h3>

	<ol class="commentlist">

    <?php $count = 0; foreach ($comments as $comment) : if (get_comment_type() == "comment") : $count++; ?>
		
		<li class="<?php
			/* Only use the authcomment class from style.css if the user_id is 1 (admin) */
			if (1 == $comment->user_id)
			$oddcomment = "authcomment";
			echo $oddcomment;
			?>" id="comment-<?php comment_ID() ?>">

			<cite><?php comment_author(); ?></cite>
			<?php if ($comment->comment_approved == '0') : ?>
			<em><?php _e('Yor comment is waiting for moderation.','ckg-media'); ?></em>
			<?php endif; ?>
			<br />

			<small class="commentmetadata"><a href="#comment-<?php comment_ID() ?>" title=""><?php comment_date('j. F Y') ?> - <?php comment_time() ?></a><?php edit_comment_link(__(' - edit comment','ckg-media'),'',''); ?></small>

			<?php comment_text() ?>

		</li>

    <?php endif; endforeach; /* end for each comment */ ?>

	</ol>

 <?php else : // this is displayed if there are no comments so far ?>

  <?php if ('open' == $post-> comment_status) : ?> 
		<!-- If comments are open, but there are no comments. -->
		
	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="nocomments"><?php _e('Comments are closed.','ckg-media'); ?></p>
		
	<?php endif; ?>
<?php endif; ?>


<?php if ('open' == $post-> comment_status) : ?>

<h3 id="respond"><?php _e('Leave a comment','ckg-media'); ?></h3>

<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<p><?php _e('You have to be','ckg-media'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>"><?php _e('logged in','ckg-media'); ?></a>, <?php _e('to leave a comment.','ckg-media'); ?></p>
<?php else : ?>

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

<?php if ( $user_ID ) : ?>

<p><?php _e('Logged in as','ckg-media'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="<?php _e('Log out &raquo;','ckg-media') ?>"><?php _e('Log out &raquo;','ckg-media'); ?></a></p>

<?php else : ?>

<p><input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" />
<label for="author"><small><?php _e('Name','ckg-media'); ?> <?php if ($req) _e('(required)','ckg-media'); ?></small></label></p>

<p><input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" />
<label for="email"><small><?php _e('E-mail (will not be published)','ckg-media'); ?> <?php if ($req) _e('(required)','ckg-media'); ?></small></label></p>

<p><input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="22" tabindex="3" />
<label for="url"><small><?php _e('Website','ckg-media'); ?></small></label></p>

<?php endif; ?>

<p>
	<small>
		<?php _e('You can use the following tags:','ckg-media'); ?> <?php echo allowed_tags(); ?>
	</small>
</p>

<p><textarea name="comment" id="comment" cols="100%" rows="10" tabindex="4"></textarea></p>

<p><input name="submit" type="submit" id="submit" tabindex="5" value=<?php _e('Submit comment','ckg-media'); ?> />
<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
</p>
<?php do_action('comment_form', $post->ID); ?>

</form>

<?php endif; // If registration required and not logged in ?>

<?php endif; // if you delete this the sky will fall on your head ?>